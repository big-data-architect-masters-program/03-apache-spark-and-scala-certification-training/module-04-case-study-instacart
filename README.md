# Module 04 Case Study - Instacart (in progress)

## Domain: E-commerce
Instacart is a grocery ordering and delivery app that aims to make it easy to fill your refrigerator and pantry with your personal favorites and staples when you need them. Instacart’s data science team plays a big part in providing this delightful shopping experience. Currently,they use transactional data to develop models that predict which products a user will buy again, try for the first time, or add to their cart next during a session.

The dataset is a relational set of files describing customers' orders over time. The dataset is anonymized and contains a sample of over 3 million grocery orders from more than 200,000 Instacart users.

__Tasks:__

1. Transfer complete dataset from RDBMS to HDFS
2. Validate the loaded data by comparing the statistics of data both in source and HDFS
3. Create a new directory in HDFS named cheeses and load only rows where aisle is "specialty cheeses"
4. update "specialty cheeses" to "specialtycheese" and transfer only updated rows in the above-created directory

__Dataset:__ You can download the required dataset fromyour LMS.